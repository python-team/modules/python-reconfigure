#!/bin/sh -e

GH_PROJECT=Eugeny/reconfigure
PY_NAME=reconfigure

# receive the list of changesets where the version changed, take only the first one
GIT_REV=$(GET -o links https://github.com/${GH_PROJECT}/commits/master/${PY_NAME}/__init__.py 2>/dev/null 2>/dev/null | grep -o "https://github.com/${GH_PROJECT}/tree/.*/${PY_NAME}/__init__.py" -m 1 | sed -e 's=^.*tree/\([0-9a-f]*\)/.*=\1=g')

GIT_REV_SHORT=$(echo ${GIT_REV} | grep -E -o '.{8}' | head -n1)

VERSION=$(GET https://raw.githubusercontent.com/${GH_PROJECT}/${GIT_REV}/${PY_NAME}/__init__.py 2>/dev/null | grep -E -o '[[:digit:]]+\.[[:digit:]]+(\.[[:digit:]]+)?')

PACKAGE=$(dpkg-parsechangelog -S Source)

GET https://github.com/${GH_PROJECT}/archive/${GIT_REV}.tar.gz > ${PACKAGE}_${VERSION}+git${GIT_REV_SHORT}.orig.tar.gz
