Source: python-reconfigure
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Build-Depends:
    dh-python,
    python3-setuptools (>= 0.6.24),
    python3-all,
    python3-sphinx,
    python3-chardet,
    python3-nose,
    python3-iniparse,
    debhelper-compat (= 9)
Standards-Version: 3.9.4
Homepage: http://eugeny.github.io/reconfigure
Vcs-Git: https://salsa.debian.org/python-team/packages/python-reconfigure.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-reconfigure

Package: python-reconfigure-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: simple config file management library (documentation)
 python-reconfigure provides easy and uniform access to various
 kinds of configuration files, easily extendable with custom
 parsers.
 .
 This package provides documentation to python-reconfigure.

Package: python3-reconfigure
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-iniparse
Description: simple config file management library (Python 3)
 python-reconfigure provides easy and uniform access to various
 kinds of configuration files, easily extendable with custom
 parsers.
 .
 Supported configuration files:
  * Ajenti (ajenti)
  * BIND9 DNS (bind9)
  * Crontabs (crontab)
  * Samba CTDB (ctdb)
  * ISC DHCPD / uDHCPD (dhcpd)
  * NFS /etc/exports (exports)
  * /etc/fstab (fstab)
  * /etc/group (group)
  * /etc/hosts (hosts)
  * iptables-save dump (iptables)
  * Netatalk afp.conf (netatalk)
  * NSD DNS (nsd)
  * /etc/passwd (passwd)
  * /etc/resolv.conf (resolv)
  * Samba (samba)
  * Squid 3 (squid)
  * Supervisord (supervisor)
 .
 Included parsers:
  * BIND9 config (bind9)
  * Crontab (crontab)
  * NFS Exports (exports)
  * .ini (ini)
  * iptables-save (iptables)
  * nginx-like (nginx)
  * squid (squid)
  * nsd (nsd)
  * CSV-like space-separated values (ssv)
  * JSON (jsonparser)
 .
 This is a Python 3 version of the package
